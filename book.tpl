{include file="header.tpl"}

{if $book ne ''}
<div class="book">
<div style="float:left">
	<img class="img-rounded" style="margin:10px" src="/images/full/{$book.SmallImageURL}" />
</div>
<div style="float:left;width:300px;margin:10px">
	<h2>{$book.DisplayName}</h2>
	<h3>{$book.ProductID}</h3>
	<p>{$book.LongDetail}</p>
	<a class="btn btn-primary" href="https://shop.bydesign.com/UsborneBooksAndMore/#/shop/detail/{$book.ProductID}/from/{$smarty.get.cid}" role="button">See This Book On Usborne</a>
</div>
</div>
{/if}

{include file="footer.tpl"}