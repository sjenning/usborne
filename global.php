<?php

use Elasticsearch\ClientBuilder;
require 'vendor/autoload.php';
require 'vendor/smarty/smarty/libs/Smarty.class.php';

//ini_set('display_errors', 'On');
//error_reporting(E_ALL | E_STRICT);

if (!isset($_ENV['ES_HOST']) || $_ENV['ES_HOST'] == "" ||
	!isset($_ENV['DEFAULT_CONSULTANT_ID']) || $_ENV['DEFAULT_CONSULTANT_ID'] == "" ||
	!isset($_ENV['DEFAULT_CONSULTANT_NAME']) || $_ENV['DEFAULT_CONSULTANT_NAME'] == "" ||
	!isset($_ENV['SITEURL']) || $_ENV['SITEURL'] == "") {
	require 'config.php';
} else {
	define('ES_HOST', $_ENV['ES_HOST']);
	define('DEFAULT_CONSULTANT_ID', $_ENV['DEFAULT_CONSULTANT_ID']);
	define('DEFAULT_CONSULTANT_NAME', $_ENV['DEFAULT_CONSULTANT_NAME']);
	define('SITEURL', $_ENV['SITEURL']);
}

$client = ClientBuilder::create()->setHosts([ES_HOST])->build();

date_default_timezone_set('US/Central');

$smarty = new Smarty;
$smarty->setCompileDir("/tmp");
$smarty->setCacheDir("/tmp");

$smarty->assign('msgclass', '');
$smarty->assign('msg', '');
$smarty->assign('q', '');

?>