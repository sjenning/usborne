<?php

require 'global.php';

if (!isset($_GET['cid']) || $_GET['cid'] == "" || !isset($_GET['name']) || $_GET['name'] == "") {
	header("Location: $SITEURL/search.php?cid=".DEFAULT_CONSULTANT_ID."&name=".urlencode(DEFAULT_CONSULTANT_NAME), true, 302);
	die();
}

$smarty->assign('results', array());

if(isset($_GET['q']) && $_GET['q'] != "") {
	if(!preg_match('/[^ 0-9A-Za-z]/', $_GET['q'])) {
		$params = [
			'index' => 'usborne',
			'type' => 'book',
			'body' => [
				'min_score' => 0.3,
				'size' => 50,
				'query' => [
					'multi_match' => [
						'fields' => ['DisplayName', 'LongDetail'],
						'query' => $_GET['q'],
						'fuzziness' => 'auto',
						'prefix_length' => 3
					]
				]
			]
		];

		$response = $client->search($params);
		//print_r($response['hits']);
		if($response['hits']['total'] == 0) {
			$smarty->assign('msgclass', 'alert-warning');
			$smarty->assign('msg', 'No search results found');
		}
		$smarty->assign('results', $response['hits']['hits']);
		$smarty->assign('q', $_GET['q']);
	} else {
		$smarty->assign('msgclass', 'alert-danger');
		$smarty->assign('msg', 'Search query can only be letters, numbers, and spaces');
	}
} else {
	$smarty->assign('msgclass', 'alert-info');
	$smarty->assign('msg', 'Use the search bar above to begin');	
}

$smarty->display('search.tpl');

?>