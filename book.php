<?php

require 'global.php';

if (!isset($_GET['cid']) || $_GET['cid'] == "" || !isset($_GET['name']) || $_GET['name'] == "") {
	header("Location: $SITEURL/book.php?cid=".DEFAULT_CONSULTANT_ID."&name=".urlencode(DEFAULT_CONSULTANT_NAME)."&id=".$_GET['id'], true, 302);
	die();
}

$smarty->assign('book', '');

if(isset($_GET['id']) && $_GET['id'] != "") {
	if(!preg_match('/[^-0-9A-Za-z]/', $_GET['id'])) {
		$params = [
			'index' => 'usborne',
			'type' => 'book',
			'body' => [
				'filter' => [
					'term' => [
						'ProductID' => $_GET['id']
					]
				]
			]
		];

		$response = $client->search($params);
		//print_r($response);
		if($response['hits']['total'] == 0) {
			$smarty->assign('msgclass', 'alert-warning');
			$smarty->assign('msg', 'Book not found');
		} else {
			$smarty->assign('book', $response['hits']['hits'][0]['_source']);
		}
	} else {
		$smarty->assign('msgclass', 'alert-danger');
		$smarty->assign('msg', 'Invalid book ID format');
	}
} else {
	$smarty->assign('msgclass', 'alert-danger');
	$smarty->assign('msg', 'No book ID provided');
}

$smarty->display('book.tpl');

?>